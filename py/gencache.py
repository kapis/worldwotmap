#!/usr/bin/env python3

import plyvel, json, urllib.request, networkx, random, math, re, utils
from math import *
from utils import *

CESIUMPLUS_URL = "https://g1.data.le-sou.org"
DUNITER_PATH = "~/.config/duniter/duniter_default"
OUTPUT_PATH = "www/data"
OVERLAY_PRECISION = "0.5"

NODE_OPTI_MINLAT = 0.01 # degrees (minimum latitude difference to test distance for repulsion)
NODE_MINDIST = 30 # meter

def exportJSON(outfile, data):
	f = open(outfile, "w")
	json.dump(data, f, separators=(",",":"))
	f.close()

# Get the distance (m) between two points (lat,lon) (radians) on Earth (approximated to a sphere)
# https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
def geodist(p1, p2):
	a = sin((p2[0]-p1[0])/2)**2 + sin((p2[1]-p1[1])/2)**2 * cos(p1[0]) * cos(p2[0])
	return 12742000*atan2(sqrt(a),sqrt(1-a))

MERCATOR_MAX_LAT = degrees(2*atan(e**pi)-pi/2)
def mercator(lat):
	return pi-math.log(tan(radians(lat)/2+pi/4))

# TODO support multi-issuer
REGEX_SIG = re.compile("^SIG\\(([a-zA-Z0-9]+)\\)$")
def get_tx_result(tx):
	total = 0
	results = {}
	for i in tx["outputs"]:
		elems = i.split(":")
		try:
			pubkey = REGEX_SIG.finditer(elems[2]).__next__().groups()[0]
		except:
			continue
		amount = int(elems[0])*10**int(elems[1])
		total += amount
		if pubkey in results:
			results[pubkey] += amount
		else:
			results[pubkey] = amount
	
	issuer = tx["issuers"][0]
	if issuer in results:
		results[issuer] -= total
	else:
		results[issuer] = total
	
	return results

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("""WorldWotMap data extractor
CopyLeft 2018-2021 Pascal Engélibert (GNU AGPL v3)

Duniter must not be running while extracting LevelDB data.
Only compatible with Duniter >=1.7.9.

Options:
 -c <url>   Cesium+ URL
  default: {}
 -d <path>  Duniter profile path
  default: {}
 -e <path>  Output dir
  default: {}
 -o         Do not generate image overlays
 -op <nb>   Overlay precision (degrees)
  default: {}
 -r         Do not compute node repulsion
 -v         Verbose mode
""".format(CESIUMPLUS_URL, LEVELDB_PATH, OUTPUT_PATH, OVERLAY_PRECISION))
		exit()
	
	if "-v" in sys.argv:
		utils.VERBOSITY |= LOG_TRACE
	
	output_dir = format_path(getargv("-e", OUTPUT_PATH))
	try:
		os.mkdir(output_dir)
	except FileExistsError:
		pass
	
	output = {
		"certs": [],
		"accounts": {},
		"time": time.time(),
		"current_block": {
			"number": None,
			"hash": None,
			"median_time": None
		}
	}
	
	log("Asking Cesium+")
	results = json.loads(urllib.request.urlopen(
		getargv("-c", CESIUMPLUS_URL)+"/user/profile/_search",
		b'{"query":{"bool":{"must":[{"exists":{"field":"geoPoint"}}]}},"from":0,"size":'+str(json.loads(urllib.request.urlopen(
			getargv("-c", CESIUMPLUS_URL)+"/user/profile/_search",
			b'{"query":{"bool":{"must":[{"exists":{"field":"geoPoint"}}]}},"from":0,"size":0,"_source":[]}'
		).read().decode())["hits"]["total"]).encode()+b',"_source":["title","geoPoint","avatar._content_type"]}'
	).read().decode())["hits"]["hits"]
	log("Cesium+ accounts: {}".format(len(results)), LOG_INFO)
	
	log("Opening DBs")
	duniter_path = format_path(getargv("-d", DUNITER_PATH))
	iindex = plyvel.DB(duniter_path + "data/leveldb/level_iindex")
	cindex = plyvel.DB(duniter_path + "data/leveldb/level_cindex")
	bindex = plyvel.DB(duniter_path + "data/leveldb/level_bindex")
	
	log("Fetching current block")
	b_iter = bindex.iterator()
	b_iter.seek_to_stop()
	current_block = json.loads(b_iter.prev()[1].decode())
	output["current_block"]["number"] = current_block["number"]
	output["current_block"]["hash"] = current_block["hash"]
	output["current_block"]["median_time"] = current_block["medianTime"]
	bindex.close()
	
	log("Iterating over identities")
	graph = networkx.Graph()
	account_index_by_id = []
	account_index_by_pk = {}
	i = 0
	for pub, row in iindex:
		idty = json.loads(row.decode())[0]
		certs = json.loads(cindex.get(pub).decode())
		
		output["accounts"][idty["pub"]] = [[idty["uid"], idty["member"], None], None]
		for cert in certs["issued"]:
			if cert["expired_on"] == 0:
				output["certs"].append([idty["pub"], cert["receiver"]])
		
		account_index_by_id.append(idty["pub"])
		account_index_by_pk[idty["pub"]] = i
		
		i += 1
	
	iindex.close()
	cindex.close()
	log("Identities: {}".format(len(output["accounts"])), LOG_INFO)
	log("Certifications: {}".format(len(output["certs"])), LOG_INFO)
	
	log("Computing communities")
	graph.add_nodes_from(range(0, len(output["accounts"])))
	for cert in output["certs"]:
		graph.add_edge(account_index_by_pk[cert[0]], account_index_by_pk[cert[1]])
	communities = [i for i in networkx.community.label_propagation_communities(graph)]
	
	log("Communities: {}".format(len(communities)), LOG_INFO)
	output["communities"] = len(communities)
	for community in range(len(communities)):
		for i in communities[community]:
			output["accounts"][account_index_by_id[i]][0][2] = community
	
	do_repulsion = not "-r" in sys.argv
	do_overlays = not "-o" in sys.argv
	if do_overlays:
		from PIL import Image
		windex = plyvel.DB(duniter_path + "data/leveldb/level_wallet")
		overlay_precision = float(getargv("-op", OVERLAY_PRECISION))
		overlay_size = ceil(360/overlay_precision)
	
	log("Iterating over Cesium+ accounts")
	repulsion = {}
	overlay_account_density = {}
	overlay_money_density = {}
	for account in results:
		data = [account["_source"]["title"], "avatar" in account["_source"], [account["_source"]["geoPoint"]["lat"], account["_source"]["geoPoint"]["lon"]]]
		
		if do_overlays and abs(data[2][0]) < MERCATOR_MAX_LAT:
			pos = (int((data[2][1]+180)/overlay_precision), int(mercator(data[2][0])/tau*overlay_size))
			
			if pos in overlay_account_density:
				overlay_account_density[pos] += 1
			else:
				overlay_account_density[pos] = 1
			
			wallet = windex.get(("SIG({})".format(account["_id"])).encode())
			if wallet:
				wallet = json.loads(wallet)
				if pos in overlay_money_density:
					overlay_money_density[pos] += wallet["balance"]
				else:
					overlay_money_density[pos] = wallet["balance"]
		
		if do_repulsion:
			f = [0, 0]
			for node in repulsion:
				p1 = data[2]
				p2 = output["accounts"][node][1][2]
				
				# geodist is expensive; avoid calling it when result is obvious
				if abs(p1[0]-p2[0]) > NODE_OPTI_MINLAT:
					continue
				
				p1 = [radians(i) for i in p1]
				p2 = [radians(i) for i in p2]
				
				if abs(p1[0]-p2[0]) < 1e-5 and abs(p1[1]-p2[1]) < 1e-5:
					p1[0] += (random.random()-.5)*1e-5
					p1[1] += (random.random()-.5)*1e-5
				
				d = geodist(p1, p2)
				if d < NODE_MINDIST:
					# angle between P1 and P2
					angle = acos(cos(p2[0]-p1[0]) * cos(p2[1]-p1[1]))
					
					# convert P1 and P2 to cartesian coords
					p1c = [sin(p1[0])*cos(p1[1]), sin(p1[0])*sin(p1[1]), cos(p1[0])]
					p2c = [sin(p2[0])*cos(p2[1]), sin(p2[0])*sin(p2[1]), cos(p2[0])]
					
					# P1*P2 vector product
					vp = [p1c[1]*p2c[2]-p1c[2]*p2c[1], p1c[2]*p2c[0]-p1c[0]*p2c[2], p1c[0]*p2c[1]-p1c[1]*p2c[0]]
					# |P1*P2|
					vpd = sqrt(vp[0]**2 + vp[1]**2 + vp[2]**2)
					if vpd == 0:
						print("p1 = ", p1)
						print("p2 = ", p2)
						print("p1c = ", p1c)
						print("p2c = ", p2c)
						print("vp = ", vp)
					# P1*(P1*P2) vector product
					vpp = [p1c[1]*vp[2]-p1c[2]*vp[1], p1c[2]*vp[0]-p1c[0]*vp[2], p1c[0]*vp[1]-p1c[1]*vp[0]]
					# V = P1*(P1*P2) / |P1*P2|
					v = [vp[0]/vpd, vp[1]/vpd, vp[2]/vpd]
					
					# great circle passing through P1 and P2 is C(t)=cos(t)*P1+sin(t)*V
					fa = (NODE_MINDIST - d) / 63781.34#6378134
					f1c = [cos(fa)*p1c[0]+sin(fa)*v[0], cos(fa)*p1c[1]+sin(fa)*v[1], cos(fa)*p1c[2]+sin(fa)*v[2]]
					f1 = [acos(f1c[2]), atan2(f1c[1], f1c[0])] # new P1 to sperical coords
					f[0] += f1[0] - p1[0]
					f[1] += f1[1] - p1[1]
					
					fa = -angle-fa
					f2c = [cos(fa)*p2c[0]+sin(fa)*v[0], cos(fa)*p2c[1]+sin(fa)*v[1], cos(fa)*p2c[2]+sin(fa)*v[2]]
					f2cd = sqrt(f2c[0]**2 + f2c[1]**2 + f2c[2]**2)
					f2c = [i/f2cd for i in f2c]
					f2 = [acos(f2c[2]), atan2(f2c[1], f2c[0])] # new P2 to sperical coords
					repulsion[node][0] += f2[0] - p2[0]
					repulsion[node][1] += f2[1] - p2[1]
			
			repulsion[account["_id"]] = f
		
		if account["_id"] in output["accounts"]:
			output["accounts"][account["_id"]][1] = data
		else:
			output["accounts"][account["_id"]] = [None, data]
	
	for node in repulsion:
		pos = output["accounts"][node][1][2]
		pos[0] += repulsion[node][0]
		pos[1] += repulsion[node][1]
	
	log("Total accounts: {}".format(len(output["accounts"])), LOG_INFO)
	
	exportJSON(output_dir+"duniterdb.json", output)
	
	if do_overlays:
		log("Generating overlays")
		
		# Account density
		maxd = overlay_account_density[max(overlay_account_density, key=lambda p: overlay_account_density[p])]
		img = Image.new("RGBA", (overlay_size, overlay_size))
		for y in range(overlay_size):
			for x in range(overlay_size):
				img.putpixel((x,y), (255,0,0,32+int(overlay_account_density[(x,y)]/maxd*223)) if (x,y) in overlay_account_density else (0,0,0,0))
		img.save(output_dir+"overlay_account_density.png");
		
		# Money density
		maxd = overlay_money_density[max(overlay_money_density, key=lambda p: overlay_money_density[p])]
		img = Image.new("RGBA", (overlay_size, overlay_size))
		for y in range(overlay_size):
			for x in range(overlay_size):
				img.putpixel((x,y), (0,0,255,32+int(overlay_money_density[(x,y)]/maxd*223)) if (x,y) in overlay_money_density else (0,0,0,0))
		img.save(output_dir+"overlay_money_density.png");
		
		# Transaction density
		overlay_tx_density = {}
		block_archive = [int(i.split("_")[1].split("-")[0]) for i in os.listdir(duniter_path+"g1")]
		block_archive.sort(reverse=True)
		from_time = current_block["medianTime"] - 31557600
		loop = True
		for f in block_archive:
			if not loop:
				break
			f = open(duniter_path+"g1/chunk_{}-250.json".format(f), "r")
			blocks = json.load(f)
			for block in blocks["blocks"]:
				if block["medianTime"] < from_time:
					loop = False
					continue
				for tx in block["transactions"]:
					tx_result = get_tx_result(tx)
					for pubkey in tx["issuers"]:
						if pubkey in output["accounts"]:
							account = output["accounts"][pubkey]
							if account[1]:
								pos = (int((account[1][2][1]+180)/overlay_precision), int(mercator(account[1][2][0])/tau*overlay_size))
								
								if pos in overlay_tx_density:
									if tx_result[pubkey] > 0:
										overlay_tx_density[pos][0] += tx_result[pubkey]
									else:
										overlay_tx_density[pos][1] -= tx_result[pubkey]
								else:
									overlay_tx_density[pos] = [tx_result[pubkey],0] if tx_result[pubkey]>0 else [0,-tx_result[pubkey]]
			f.close()
		
		maxd = sum(overlay_tx_density[max(overlay_tx_density, key=lambda p: sum(overlay_tx_density[p]))])
		img = Image.new("RGBA", (overlay_size, overlay_size))
		for y in range(overlay_size):
			for x in range(overlay_size):
				if (x,y) in overlay_tx_density:
					v = overlay_tx_density[(x,y)]
					s = sum(v)
					r = 255 if s == 0 else int(v[0]/s*510)
					img.putpixel((x,y), (min(255,r),min(255,510-r),0,64+int(s/maxd*191)))
				else:
					img.putpixel((x,y), (0,0,0,0))
		img.save(output_dir+"overlay_tx_density.png");


"""
pubkey => [[pseudo:str, member:bool, community:uint]?, [title:str, avatar:bool, [lat:float, lon:float]]?]
"""
