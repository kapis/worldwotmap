#!/usr/bin/env python3

import distutils.dir_util
from utils import *

DEFAULT_LANG = "fr"
OUTPUT_PATH = "www"

LOCALES_PATH = "locale"
STATIC_PATH = "static"
TEMPLATES_PATH = "templates"
VERSION = "0.8.0"
COPY_YEARS = "2018-2021"

def gen_tr(locale, output_path, consts):
	for template in os.listdir(TEMPLATES_PATH):
		f = open(TEMPLATES_PATH+"/"+template, "r")
		raw = f.read()
		f.close()
		
		for tr in locale:
			raw = raw.replace("{{"+tr+"}}", locale[tr])
		
		for const in consts:
			raw = raw.replace("{{"+const+"}}", consts[const])
		
		f = open(output_path+template, "w")
		f.write(raw)
		f.close()

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("""WorldWotMap static webpage generator
CopyLeft 2021 Pascal Engélibert (GNU AGPL v3)

Options:
 -d <lang>  Default & fallback language (default=fr)
 -o <path>  Output path (default=www)
""")
		exit()
	
	if "-v" in sys.argv:
		VERBOSITY |= LOG_TRACE
	
	default_lang = getargv("-d", DEFAULT_LANG)
	output_path = format_path(getargv("-o", OUTPUT_PATH))
	
	try:
		os.mkdir(output_path)
	except FileExistsError:
		pass
	
	locales = {}
	for locale in os.listdir(LOCALES_PATH):
		if not locale.endswith(".tr.py"):
			continue
		
		f = open(LOCALES_PATH+"/"+locale, "r")
		locale = eval(f.read())
		f.close()
		
		locales[locale["lang"]] = locale
	
	lang_options = lambda lang, base_path: "".join(['<option value="{}{}"{}>{}</option>'.format(base_path, i, " selected" if i==lang else "", locales[i]["lang-name"]) for i in locales])
	
	f = open(LOCALES_PATH+"/"+default_lang+".tr.py", "r")
	default_locale = eval(f.read())
	f.close()
	
	gen_tr(default_locale, output_path, {"base-path": "", "lang-options": lang_options(default_lang, ""), "version": VERSION, "copy-years": COPY_YEARS})
	
	for lang in locales:
		locale = locales[lang]
		locale_path = output_path+lang+"/"
		
		try:
			os.mkdir(locale_path)
		except FileExistsError:
			pass
		
		# Add fallback translations
		for tr in default_locale:
			if not tr in locale:
				locale[tr] = default_locale[tr]
		
		# Generate files
		gen_tr(locale, locale_path, {"base-path": "../", "lang-options": lang_options(lang, "../"), "version": VERSION, "copy-years": COPY_YEARS})
	
	distutils.dir_util.copy_tree(STATIC_PATH, output_path)
